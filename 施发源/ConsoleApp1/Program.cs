﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //foreach
            string[] Asd = { "啥也不会", "啥也不懂", "啥也不问", "差不多是这样" };

            foreach (string i in Asd)
            {
                Console.WriteLine(i);
            }
                

           


            //for
            double[,] Cj = { { 1, 98.5 }, { 2, 60.5 }, { 3, 85 } };
            for (int i = 0; i < Cj.GetLength(0); i++)
            {
                Console.Write($"第{Cj[i,0]}个同学的成绩：");
                for (int j = 1; j < Cj.GetLength(1); j++)
                {
                    Console.Write(Cj[i, j]);
                }
                Console.WriteLine();
            }

            //Split
            //单字符分割
            string fg = "啥也不会，啥也不懂，啥也不问，差不多就这样了";
            Console.WriteLine($"原字符：{fg}");
            string[] fgh = fg.Split('，');
            Console.Write($"处理后：");
            foreach (string i in fgh)
            {
                Console.Write(i);
            }
            Console.WriteLine();
            //多字符分割
            string fgs = "asdfghjklqwertyuiop";
            Console.WriteLine($"原字符{fgs}");
            string[] fgsh = fgs.Split(new char[5] {'f','g','h','t','y'});
            Console.Write($"处理后：");
            foreach (string i in fgsh)
            {
                Console.Write(i);
            }
        }
    }
}
