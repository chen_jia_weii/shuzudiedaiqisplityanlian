﻿using System;
using System.Text.RegularExpressions;

namespace DemoNum
{
    class Program
    {
        static void Main(string[] args)
        {



            string[,,,] g = new string[2, 2, 4, 2]  //四维数组
       {
              {{{"玫","瑰"},{"你","在"},{"哪","里" },{"你","说" }},
              {{"你","爱" },{"过","的" },{"人","都" },{"已","经" }}},

              {{{"离","去" },{"不","要" },{"欺","骗" },{"自","己" }},
              {{"你","只是"},{"隐藏","的" },{"比较","深" },{"而","已" }}}
       };
            g.PrintArrayInfo();

            Console.WriteLine();

            //Split  用单个字符截取
            string book = "张爱玲:《倾城之恋》,村上春树:《挪威的森林》,德克旭贝里:《小王子》";
            char[] fArray = { ',' };
            var a = book.Split(fArray);
            a.PrintArrayInfo();

            Console.WriteLine();
            // 用多个单字符截取
            string[] nArray =book.Split(new char[3] { ',', '《', '》' });
            nArray.PrintArrayInfo();


            //用字符串截取
            string str = "我是一个保安，保卫一方平安，生活郁郁寡欢，上班只为下班";
            string[] sArray = Regex.Split(str, "我是", RegexOptions.IgnoreCase);

            //用foreach迭代
            foreach (string item in sArray) Console.Write(item.ToString() + "立正   ");
        }
    }
}
