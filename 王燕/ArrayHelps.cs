﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DemoNum
{
    public static class ArrayHelps
    {

        //扩展方法 引用
        public static void PrintArrayInfo(this string[] arr)
        {
            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }
        }

        public static void PrintArrayInfo(this int[,] arr)
        {

            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.Write(arr[i, j] + " ");
                }
            }
        }

        public static void PrintArrayInfo(this string[,,,] arr)
        {
            Console.WriteLine();
            foreach (var item in arr)
            {
                Console.Write(item + " ");
            }
        }
    }
}
