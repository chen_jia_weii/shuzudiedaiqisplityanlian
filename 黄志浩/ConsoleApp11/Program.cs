﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            //数组求最大最小值


            int[] a = new int[6];
            Console.WriteLine("请输入六个数");
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("请输入第{0}个数：",i+1);
                a[i]= int.Parse(Console.ReadLine());
                
            }
            int max = a[0];
            int min = a[0];
            for (int i = 1; i < a.Length; i++)
            {
                if (max < a[i])
                {
                    max = a[i];
                }
                else if (min > a[i])
                {
                    min = a[i];
                }
            }
            Console.WriteLine("最大值为：{0}",max);
            Console.WriteLine("最小值为：{0}：",min);

            //foreash 用法
            string str1 = "这会儿终于轮到我了吧";
            foreach (var item in str1)
            {
                Console.WriteLine(item);
            }


            //spilt 演示
            string str = "这 怎么 用 不 了";
            string[] result = str.Split(' ');
            Console.WriteLine(result);
            foreach (var intem in str)
            {
                Console.WriteLine(intem);
            }
            Console.WriteLine();
        }
    }
}
