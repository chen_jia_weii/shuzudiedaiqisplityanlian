﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] array =
            {
                "圣光背叛了我",
                "我的圣光啊",
                "我不会说的",
            };
            array.PrintArray();
            int[] array01 = new int[5];
            for (int i=0;i<array01.Length; i++)
            {
                array01[i] = i;
                Console.WriteLine(array01[i]);
            }
            /*Split的用法*/
            string attempt = "我不会说的，我是马尔噶内斯，我是不朽的";
            string[] array02 = attempt.Split('我','很','，');
            foreach (var ieme in array02)
            {
                Console.WriteLine(ieme);
            }
        }
    }
}
