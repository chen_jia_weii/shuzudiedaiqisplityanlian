﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public static class ArrayHelper
    {
        public static void PrintArray(this string[] arr)
        {
            for (int i =0; i<arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            };
        }
    }
}
