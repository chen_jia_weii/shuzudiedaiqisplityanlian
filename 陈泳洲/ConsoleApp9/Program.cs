﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Program
    {
        static void Main(string[] args)
        {
            string[][] array = new string[5][];
            array[0] = new string[] {"不知天高地厚","还在这","根本没把我放在眼里","准备捉妖！"};
            array[1] = new string[] { "大威天龙", "大罗法咒", "世尊地藏", "般若诸佛"};
            array[2] = new string[] { "金山法寺","妖孽禁地","般若巴嘛哄"};
            array[3] = new string[] { "大威天龙", "飞龙在天", "逆天而行", "死路一条", "诱惑众生", "应得惩罚" };
            array[4] = new string[] { "艳阳天那么风光好","红的花是绿的草","我乐乐呵呵向前跑","踏遍青山人未老"};


            for (int i = 0; i < array.Length; i++)
            {
                for (int k = 0; k < array[i].Length; k++)
                {
                    Console.WriteLine(array[i][k]);
                }
                Console.WriteLine();
            }

            string str = "他妈的,这种话怎么会从你的嘴巴里嘬出来啊！";
            String[] strNew = str.Split(',');
            foreach (var text in strNew)
            {
                Console.WriteLine(text);
            }

            Console.ReadKey();
        }
    }
}
