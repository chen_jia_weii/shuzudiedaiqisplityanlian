﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LtrativeArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[5];
            for(int num = 0; num < arr.Length;num++)
            {
                arr[num]=num;
            }
            Console.WriteLine("该数组中的元素分别是:");
            foreach (int item in arr)
            {
                
                Console.WriteLine(item);
            }
           
            Console.WriteLine("---------------");
            string[] str = new string[] { "语文", "数学", "英语" };
            Console.WriteLine("该数组中的元素分别是:");

            foreach (string a in str)
            {
            
                Console.WriteLine(a);
            }
            Console.WriteLine("---------------");
           
            string str2 = "aaatbbbsccc";
            string []strArry = str2.Split(new char[] { 't', 's' });
            foreach(string item2 in strArry)
            {
                Console.WriteLine(item2);
            }
            
            


        }
    }
}
