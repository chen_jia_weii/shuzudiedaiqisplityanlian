﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] array =
            {
                "快乐一加一",
                "快乐二加一",
                "快乐三加一",
            };
            array.PrintArray();
            int[] array01 = new int[5];
            for (int i=0;i<array01.Length; i++)
            {
                array01[i] = i;
                Console.WriteLine(array01[i]);
            }
            /*Split的用法*/
            string attempt = "我很喜欢，我又很害怕，我也很无奈";
            string[] array02 = attempt.Split('我','很','，');
            foreach (var ieme in array02)
            {
                Console.WriteLine(ieme);
            }
        }
    }
}
