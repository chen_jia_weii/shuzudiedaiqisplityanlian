﻿using System;

namespace ArrAppTest
{
    class Program
    {
        static void Main(string[] args)
        {

            //foreach迭代
            string[] carBrand = { "Ferrari", "Porsche", "Maserati", "Tesla" };

            foreach (string a in carBrand)

                Console.WriteLine(a);

            int[] myInts = { 3999999, 2999999, 3888889, 780000 };

            foreach (int i in myInts)

                Console.WriteLine(i);


            //for迭代
            double[,] moneys = { { 900000, 100000 }, { 9999999999, 199999999999 }, { 899999999999.99, 19999999999999.99 } };
            for (int i = 0; i < moneys.GetLength(0); i++)
            {
                Console.WriteLine("第" + (i + 1) + "一辆车的厂家与实体报价：");
                for (int j = 0; j < moneys.GetLength(1); j++)
                {
                    Console.Write(moneys[i, j] + "，");
                }
                Console.WriteLine();
            }

            //Split
            //1.对单个字符进行分割
            string stence = "手心的蔷薇，刺伤而不知觉，你值得被疼爱，你懂我的期待";
            string[] sStence = stence.Split('，');
            foreach (string i in sStence)
            {
                Console.WriteLine(i.ToString());
            }

            //2.对多个字符进行分割
            string stence1 = "abcdefghijklnm";
            string[] sStence1 = stence1.Split(new char[3] { 'c', 'h', 'l' });
            foreach (string i in sStence1)
            {
                Console.Write(i.ToString() + " ");
            }

            Console.ReadKey();
        }
    }
}
