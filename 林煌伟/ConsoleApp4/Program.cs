﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            //一维数组的定义和使用
            
            int[] arr = new int[3] { 20, 50, 70 };

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }
            Console.WriteLine();

            //多维数组的定义和使用
            
            int[][] arr1 = new int[2][];
            arr1[0] = new int[] { 2, 4, 6 };
            arr1[1] = new int[] { 20, 50, 86};
            
            for (int n = 0; n < arr1.Length; n++)
            {
                for (int m = 0; m < arr1[n].Length; m++)
                {
                    Console.WriteLine(arr1[n][m]);
                }
            }
            Console.WriteLine();

            //foreach的使用
            
            string[] arr2 = new string[3] {"天","地","人" };
            string sum = "";
            foreach(string num in arr2)
            {
                sum += num;
            }
            Console.WriteLine(sum);
            Console.WriteLine();
           
            //split字符串分隔方法的使用
            
            string num1 = "执子之手，与子偕老";
            char[] sArr = { '，'};
            string[] stArr = num1.Split(sArr);

            foreach (string sArr2 in stArr)
            {
                Console.WriteLine(sArr2);
            }

            Console.WriteLine();

            string[] sArr1 = num1.Split(sArr, StringSplitOptions.None);

            foreach (string sArr3 in sArr1)
            {
                Console.WriteLine(sArr3);
            }

            Console.WriteLine();

            //扩展方法的使用
            string[] strArr = {"天堂","地狱","人间" };
            strArr.StrArr();



        }
    }
}
