﻿using System;

namespace Array
{
    class Program
    {
        static void Main(string[] args)
        {
            //一维
            int[] arr1 = new int[10];//第一种

            string[] str1 = new string[] { "香蕉", "苹果", "草莓" };

            double[] doubleArray1 = { 66.6, 77.7, 88.8 };

            for(int i=0; i<str1.Length; i++ )
            {
                Console.WriteLine(str1[i]);//输出数组 
            }

            Console.WriteLine();
            //多维数组
            //创建一个二维数组
            string[,] strArr1 =
            { 
                {"床前明月光"},
                {"疑是地上霜"},
                {"举头望明月"},
                {"低头思故乡"},
            };

            //打印二维数组
            for (int j=0;j<strArr1.GetLength(0);j++)
            {
                for (int k = 0; k < strArr1.GetLength(1); k++)
                {
                    //Console.WriteLine(j+1 +"行");
                    Console.WriteLine(strArr1[j, k]);
                }
            }
            //GetLength(0)是计算二维数组的行数，GetLength(1)是计算二维数组的列数
            Console.WriteLine();
            //新建ArrayHelper类输出二维数组 
            string[] strArr2 =
            {
                 "红豆生南国" ,
                 "春来发几枝" ,
                 "愿君多采撷" ,
                 "此物最相思" ,
            };
            strArr2.PrintArrInfo();

            Console.WriteLine();
            //foreach 循环
            foreach(var item in strArr1)//item 适用于所有字符串
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            string str3 = " 我爱吃香蕉, 我爱吃桃子, 我爱吃草莓";

            foreach (var item in str3)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            double[] points = { 90, 91, 92, 93, 94, 95, 96.5 };
            double sum = 0;
            double avg = 0;
            foreach (double point in points )
            {
                sum+=point;
            }
            avg = sum / points.Length;
            Console.WriteLine(sum);
            Console.WriteLine(avg.ToString("0.00"));

            Console.WriteLine();
            //Split  

            var firstArray = str3.Split('我');

            firstArray.PrintArrInfo();

            Console.WriteLine();
            var secondArray = str3.Split(',', '爱');

            secondArray.PrintArrInfo();

        }
    }
}
