﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayText
{
    class LinearArrayAndTowArray
    {
        public void LinearArray1()
        {   //第一种方法
            int[] students = { 3, 324, 34, 5436, 45536, 577658, 23524, 4523, 123 };
            for (int i = 0; i <students.Length ; i++)
            {
                Console.WriteLine(students[i]);
            }
        }
        public void LinearArray2()
        {
            //第二种方法
            int[] student1 = new int[5];
            for (int i = 0; i < student1.Length; i++)
            {
                Console.WriteLine("请输入第" + (i+1) + "位学生的学号");
                var input = Console.ReadLine();
                student1[i] = Convert.ToInt32(input);
            }
            for (int i = 0; i < student1.Length; i++)
            {
                Console.WriteLine("第"+(i+1)+"位学生的学号为"+student1[i]);
            }
        }
        public void TowArray1()
        {
            //第一种方法
            string[,] student = new string[2, 3];
            for (int i = 0; i < student.GetLength(0); i++)
            {
                for (int j = 0; j <student.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        Console.WriteLine("请输入学生"+(1+i)+"的姓名");
                        student[i, j] = Console.ReadLine();
                    }
                    else if (j == 1)
                    {
                        Console.WriteLine("请输入学生"+(1+i)+"的班级");
                        student[i, j] = Console.ReadLine();
                    }
                    else if (j == 2)
                    {
                        Console.WriteLine("请输入学生"+(1+i)+"的学号");
                        student[i, j] = Console.ReadLine();
                    }
                }
            }
            for (int i = 0; i < student.GetLength(0); i++)
            {
                for (int j = 0; j < student.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        Console.WriteLine("第"+(i+1)+"位学生的名字为   "+student[i,j]);
                    }
                    else if (j == 1)
                    {
                        Console.WriteLine("第" + (i + 1) + "位学生的班级为   " + student[i, j]);
                    }
                    else if (j == 2)
                    {
                        Console.WriteLine("第" + (i + 1) + "位学生的名字为   " + student[i, j]);
                    }
                }
            }
        }
        public void TowArray2()
        {
            string[,] student = { { "adsgfasg","dsfsaga","awdg"}, {"dsfg","safgasafs","sdgasasdf" } };
            foreach (string input in student)
            {
                Console.WriteLine(input);
            }
        }
    }
}
