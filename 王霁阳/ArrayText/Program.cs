﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayText
{
    class Program
    {
        static void Main(string[] args)
        {
            LinearArrayAndTowArray linearArrayAndTowArray = new LinearArrayAndTowArray();
            //linearArrayAndTowArray.LinearArray1();
            //linearArrayAndTowArray.LinearArray2();
            //linearArrayAndTowArray.TowArray1();
            //linearArrayAndTowArray.TowArray2();
            //Split 的使用
            String vessel = "我爱中国,我爱中华人民共和国,我爱打游戏,我爱编程,我爱听歌";

            String[] input=vessel.Split('我','爱');
            Console.WriteLine("我爱出现了的次数为"+(input.Length-1)/2);
            foreach (var item in input)
            {
                Console.WriteLine(item);
            }
            foreach (var item in vessel)
            {
                Console.WriteLine(item);
            }
        }
    }
}
