﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //C#一维数组
            //输出第一个和最后一个
            string[] str = { "aaa", "bbb", "ccc", "ddd", "eee" };

            Console.WriteLine(str[0]);
            Console.WriteLine(str[str.Length - 1]);

            //输出数组
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i]);
            }
            //输出数组的1 3 5 个数
            for (int i = 0; i < str.Length; i = i + 2)
            {
                Console.WriteLine(str[i]);
            }

            //输入5个值储存于数组中，输出它的最小值
            int[] a = new int[5];
            Console.WriteLine("输入5个值");
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = int.Parse(Console.ReadLine());
            }
            int min = a[0];
            for (int i = 0; i < a.Length; i++)
            {
                if (a[0] < a[i])
                {
                    min = a[0];
                }

            }
            Console.WriteLine("最小值为" + min);

            //多维数组
            //定义一个多维数组，并将学生的生日输出
            int[,] b = { { 2000, 1, 12 }, { 2001, 3, 22 }, { 1999, 4, 30 }, { 2003, 9, 30 } };
            for (int i = 0; i < b.GetLength(0); i++)
            {
                Console.WriteLine("第" + (i + 1) + "个同学的生日");
                for (int j = 0; j < b.GetLength(1); j++)
                {

                    Console.Write(b[i, j] + " ");

                }
                Console.WriteLine();
            }

            //锯齿型数组
            string[][] d = new string[3][];
            d[0] = new string[] { "你是猪吗" };
            d[1] = new string[] { "你就是猪" };
            d[2] = new string[] { "哈哈哈就是你" };
            for (int i = 0; i < d.Length; i++)
            {
                Console.WriteLine("第" + (i + 1) + "行的元素");
                for (int j = 0; j < d[i].Length; j++)
                {
                    Console.Write(d[i][j] + " ");
                }
                Console.WriteLine();
            }

            //C# foreach循环用法详解
            //foreach 语句仅能用于数组、字符串或集合类数据类型。
            //foreach
            //输出成绩最高的
            double[] points = { 80, 88, 86, 90, 75.5 };
            double sum = 0;
            double max = points[0];
            foreach (double point in points)
            {
                for (int i = 0; i < points.Length; i++)
                {
                    if (points[i] > max)
                    {
                        max = points[i];
                    }
                }
            }
            Console.WriteLine("成绩最高的为"+max);
            //输出总成绩 
            foreach (double point in points)
            {
                sum = sum + point;
            }
            Console.WriteLine("总成绩" + sum);




            //C# Split：将字符串拆分为数组
            string str1 = "小时候想把头发梳成大人模样,长大了才发现大人原来没有什么头发,你选择遗忘的,是我最不舍的.";
            string[] Condition = { "," };
            string[] Result = str1.Split(Condition, StringSplitOptions.None);
            Console.WriteLine("字符串中含有的逗号个数有"+(Result.Length-1));

        }

    }
}
