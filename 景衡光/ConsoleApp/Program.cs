﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {



            Random r = new Random();
            int[] a = new int[5];   //长度为5的数组
            for (int i = 0; i < a.Length; i++)
            {
                a[i] = r.Next();  //生成五个随机数
            }
            Array.Sort(a);   //冒泡排序
            foreach (var item in a)
            {  
                Console.Write(item + ">");
            }


            Console.WriteLine();



            string[,] array =
            {
                { "关关雎鸠","在河之洲"},
                {"窈窕淑女","君子好逑"},
                {"祝胡哥：","     六一儿童节快乐" }
            };
            array.PrintArrayInfo();



            //Split
            //用单个字符分隔
            string str1 = @"
我爱你有种 左灯右行的冲突,
    疯狂却怕没有退路,
        你能否让我停止这种追逐,
            就这么双唯一的 红色高跟鞋";
            char[] charArray1 = { ',' };
            var fArray = str1.Split(charArray1);
            fArray.outputArray();

            //用多个字符分隔
            string str2 = @"
我如果爱你——
绝不像攀援的凌霄花，
借你的高枝炫耀自己；
我如果爱你——
绝不学痴情的鸟儿，
为绿荫重复单调的歌曲；
也不止像泉源，
常年送来清凉的慰藉；
也不止像险峰，
增加你的高度，衬托你的威仪。
甚至日光，
甚至春雨。";
            char[] charArray2 = new char[4] { '如', '果','，','；' };
            var divideArray = str2.Split(charArray2);
            divideArray.outputArray();


        }
    }
}
