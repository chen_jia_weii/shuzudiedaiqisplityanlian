﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp
{
      public static class Class1
    {
        
        //迭代器
        public static void PrintArrayInfo(this string[,] arr)
        {
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    Console.WriteLine(arr[i,j]);
                }
            }
        }
        
        public static void outputArray(this string[] arr)
        {
            foreach (var item in arr)
            {
                Console.WriteLine(item);
            }

        }
    }
}
