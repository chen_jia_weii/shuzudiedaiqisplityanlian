﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace work9
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 正则表达式判断邮箱格式
        foruser:
            Console.WriteLine("请输入你的邮箱：");
            string email = Console.ReadLine();
            Regex regex = new Regex(@"^(\w)+(\.\w)*@(\w)+((\.\w+)+)$");
            int firstindex = email.IndexOf("@");
            int lastindex = email.LastIndexOf("@");
            if (firstindex != -1)
            {
                if (firstindex == lastindex)
                {
                    Console.WriteLine("输入成功");
                    Console.WriteLine("你的QQ号码为：{0}", email.Substring(0, firstindex));

                }
                else
                {
                    Console.WriteLine("邮箱中含有多个@，请重新输入");
                    goto foruser;
                }
            }
            else if (!regex.IsMatch(email))
            {
                Console.WriteLine("邮箱格式错误，请重新输入");
                goto foruser;
            }
            #endregion

            Console.WriteLine();

            #region 用for、迭代器foreach打印
            double[,] points = { { 90, 80 }, { 100, 89 }, { 88.5, 86 } };
            for (int i = 0; i < points.GetLength(0); i++)
            {
                Console.WriteLine("第" + (i + 1) + "个学生成绩：\n");
                for (int j = 0; j < points.GetLength(1); j++)
                {
                    Console.WriteLine("第" + (j + 1) + "次语文成绩：" + points[i, j]);
                }
                Console.WriteLine();
            }
            Console.WriteLine("用foreach打印");
            foreach (int point in points)
            {
                Console.WriteLine(point);
            }
            Console.WriteLine();

            //锯齿形数组
            int[][] arrays = new int[3][];
            arrays[0] = new int[] { 9, 8 };
            arrays[1] = new int[] { 7, 6, 5 };
            arrays[2] = new int[] { 4, 3, 2, 1 };
            for (int a = 0; a < arrays.Length; a++)
            {
                Console.WriteLine("输出数组中第" + (a + 1) + "行的元素：");
                for (int b = 0; b < arrays[a].Length; b++)
                {
                    Console.WriteLine(arrays[a][b] + " ");
                }
            }

            Console.WriteLine("\n用foreach打印锯齿形数组：");
            for (int num = 0; num < arrays.Length; num++)
            {
                foreach (var array in arrays[num])
                {
                    Console.WriteLine(array);
                }
            }
            Console.WriteLine();
            int[][] arrays1 = new int[3][];
            arrays1[0] = new int[2];
            arrays1[1] = new int[3];
            arrays1[2] = new int[4];
            for (int a1 = 0; a1 < arrays1.Length; a1++)
            {
                Console.WriteLine("输入数组中第" + (a1 + 1) + "行的元素：");
                for (int b1 = 0; b1 < arrays1[a1].Length; b1++)
                {
                    arrays1[a1][b1] = int.Parse(Console.ReadLine());
                }
                Console.WriteLine();
            }
            Console.WriteLine("看看有没有存进去~~");
            for (int a1 = 0; a1 < arrays1.Length; a1++)
            {
                Console.WriteLine("输出数组中第" + (a1 + 1) + "行的元素：");
                for (int b1 = 0; b1 < arrays1[a1].Length; b1++)
                {
                    Console.WriteLine(arrays1[a1][b1] + " ");
                }
            }
            Console.WriteLine("\n或者用foreach打印锯齿形数组：");
            for (int num = 0; num < arrays1.Length; num++)
            {
                foreach (var array in arrays1[num])
                {
                    Console.WriteLine(array);
                }
            }
            #endregion

            Console.WriteLine();

            #region Split和扩展方法
            string str = "世间安得双全法，不负如来不负卿。";
            char[] Split = { '，', '。' };
            string[] strSplit = str.Split(Split);
            //用扩展方法打印
            PrintMethod.PrintArraysInfo(strSplit);
            #endregion

        }
    }
}
