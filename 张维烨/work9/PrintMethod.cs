﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work9
{
    public static class PrintMethod
    {   /// <summary>
    /// 只能打印string类型的数组的扩展方法
    /// </summary>
    /// <param name="array"></param>
        public static void PrintArraysInfo(this string[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }
    }
}
