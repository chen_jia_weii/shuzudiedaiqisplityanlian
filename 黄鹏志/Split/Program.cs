﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Split
{
    class Program
    {
        static void Main(string[] args)
        {       
            //固定长度的矩阵数组
            int[,] arrray1 = new int[2, 5] { 
                { 9, 9, 9, 9, 0 }, 
                { 0, 0, 9, 0, 0 }
            };
            
            Console.WriteLine("二维数组输出:  ");
            foreach (int j in arrray1)
            {
                Console.Write(j + "  ");
            }
            Console.WriteLine();
            Console.ReadLine();



            Console.WriteLine("字符串数组造字");
            Console.ReadLine();
            string[,] ru = new string[11,8]{
                {"        ","  "," ■",""," ","","■",""},
                {"       ","■","■","■","■","■","■"," "},
                {"        ","  "," ■ ","","","","■"," "},
                {"        ","   ","   ","  "," ","  "," "," "},
                {"        ","  ■ "," ",""," "," "," ",""},
                {"        "," ■"," ",""," "," "," ",""},
                {"        ","■","","","","     ■","■","■"},
                {"   ■"," ■"," ■"," ■",""," ■  ","","■"},
                {"     ","","■","   ■","","","   ■","  ■"},
                {"        ","","■","","","     ■","■","■"},
                {"     ","■","   "," ■","","","",""}
};
            for (int i = 0; i < 11; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Console.Write(ru[i, j]);
                }
                Console.WriteLine();
            }


            Console.ReadLine();
            Console.WriteLine("请输入一个含英文逗号“,”的字符串：");
            string str = Console.ReadLine();
            string[] dou = { "," };
            string[] result = str.Split(dou, StringSplitOptions.None);
            Console.WriteLine("字符串中含有逗号的个数为：" + (result.Length - 1));


        }
    }
}
