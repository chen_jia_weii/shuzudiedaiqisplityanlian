﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            //数组
            int[] arr = new int[20];
            string[] strArray = new string[]
            {
                "我爱中华人民共和国",
                "我的小布丁真可爱",
                "天气好热我要吃冰镇西瓜"
            };
            double[] doubleArray =
            {
                99.9,
                100,
                98
            };
            for(int i = 0; i < strArray.Length; i++)
            {
                Console.WriteLine(strArray[i]);
            };
            for(int n =0;n < arr.Length; n++)
            {
                Console.WriteLine(arr[n]);
            };
            //foreach迭代
            string[] stArray = { "爱分享", "爱点赞", "爱关注" };
            foreach (var item in stArray)
            {
                Console.WriteLine(item);
            }
            double[] points = { 99, 10, 56.7, 66.6 };
            double sum = 0;
            double avg = 0;
            foreach(double point in points)
            {
                sum = sum + point;
            }
            avg = sum / points.Length;
            Console.WriteLine("总成绩为：" + sum);
            Console.WriteLine("平均成绩为：" + avg);
            //Split
            string str = "我爱空调屋，我在空调屋，呆着好舒服";
            var firstArray = str.Split('空');
            foreach(string i in firstArray )
            {
                Console.WriteLine(i.ToString());
            }
            Console.WriteLine();
            char[] charArray ={ '，', '调' };
            var secondArray = str.Split(charArray);
            foreach(string a in secondArray)
            {
                Console.WriteLine(a.ToString());

            }

        }
    }
}
