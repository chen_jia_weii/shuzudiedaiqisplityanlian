﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework;

namespace Homework
{
    public static  class StringHelper
    {
        //扩展方法(针对字符串)的使用
        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '?' },
            StringSplitOptions.RemoveEmptyEntries).Length;
        }

    }
}
