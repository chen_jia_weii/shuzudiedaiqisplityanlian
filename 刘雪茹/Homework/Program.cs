﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homework;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {            
           //数组的定义
           int[] a = new int[5];
           int i, j;

           for (i = 0; i < 5; i++)
           {
               a[i] = i + 1;
           }
           for (j = 0; j < 5; j++)
           {
               Console.WriteLine("Element[{0}] = {1}", j, a[j]);
           }

           //迭代器foreach的使用
           int[,] numbers2D = { { 9, 99 }, { 3, 33 }, { 5, 55 } };
           foreach (int d in numbers2D)
           {
               System.Console.Write("{0} ", d);
           }
           Console.WriteLine();

           string str1 = "哈哈哈大笨蛋";
           foreach (var item in str1)
           {
               Console.WriteLine(item);
           }
           Console.WriteLine();

           //split字符串分割的使用
           string str2 = "你 好 s p l i t";
           string[] strArr = str2.Split(' ');
           foreach (var item in strArr)
           {
               Console.WriteLine(item);
           }
            Console.WriteLine();

            //扩展方法(针对字符串)的使用
            string s = "Hello Extension Methods?";
            int num = s.WordCount();
            Console.WriteLine("一共有{0}个元素",num);
            Console.WriteLine();

            //扩展方法(针对数组)的使用
            string[] strArray =
            {
                "蓝蓝的天",
                "白白的云",
                "绿绿的草"
            };
            strArray.PrintArraryInfo();
        }
    }
}
