﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 迭代器foreach使用
            string[] a = { "我", "喜", "爱", "与", "你" };
            for (int i = 0; i < a.Length; i = i + 2)
            {
                Console.WriteLine(a[i]);
            }
            string[] b = { "喜", "欢", "你", };
            foreach (string item in b)
            {
                Console.WriteLine(item + " ");
            }
            #endregion

            //split字符串分隔方法的使用

            string c = "杰哥快点出专辑啦" ;
            string [] d = c.Split('点','快');
            foreach (string  item in d)
            {
                Console.Write(item);
            }
            



        }
    }
}
