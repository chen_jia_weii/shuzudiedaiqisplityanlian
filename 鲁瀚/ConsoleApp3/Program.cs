﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 一维数组的定义
            int[] a = new int[4];
            int[] b;
            b = new int[2] { 1, 2 };
            int[] c = new int[3] { 1, 2, 3 };
            int[] d = { 1, 2, 3, 4, };
            #endregion


            #region 冒泡算法
            double[] t = new double[5];
            double temp;
            Console.WriteLine("输入您所查询到的考试分数");
            for (int i = 0; i < t.Length; i++)
            {
                Console.WriteLine("第" + (i + 1) + "次考试成绩");
                t[i] = double.Parse(Console.ReadLine());
            }
            //对成绩进行排序
            for (int i = 0; i < t.Length - 1; i++)
            {
                for (int j = 0; j < t.Length - 1 - i; j++)
                {
                    if (t[j] < t[j + 1])
                    {
                        temp = t[j];
                        t[j] = t[j + 1];
                        t[j + 1] = temp;
                    }
                }
            }
            Console.WriteLine("排序结果如下");
            foreach (double item in t)
            {
                Console.WriteLine(item);
            }
            #endregion


            #region 多维数组的定义
            int[,] l = { { 1, 3 }, { 3, 4 } };
            int[,] m = new int[2, 2] { { 2, 4 }, { 3, 6 } };
            int[,] h;
            h = new int[2, 2] { { 2, 4 }, { 45, 54 } };

            double[,] y = { { 66, 33 }, { 33, 43 } };
            for (int i = 0; i < y.GetLength(0); i++)
            {
                Console.WriteLine("学生成绩");
                for (int j = 0; j < y.GetLength(1); j++)
                {
                    Console.Write(y[i, j] + " ");
                }
                Console.WriteLine();
            }

            #endregion






        }
    }
}

