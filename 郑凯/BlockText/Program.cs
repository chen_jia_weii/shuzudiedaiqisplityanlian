﻿using System;

namespace BlockText
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] vs = new string[]
                {
                    "结局写在开头"
                };
            for (int i = 0; i<vs.Length; i++) 
            {
                Console.WriteLine("《" + vs[i] + "》"); 
                
            }

            string text1 = "霍金说过：即使那里成了黑洞，也是我一生想要探索的地方。";
            var firstArray = text1.Split('霍','金','说','过','：' );
            firstArray.PrintArraysInfo();
            

            string text2 = "I always know what is feeling";
            string[] strArry = text2.Split( 'f','e' );
            foreach (string item2 in strArry)
            {
                Console.WriteLine(item2);
            }
        }
    }
}
