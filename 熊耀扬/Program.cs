﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[,] arr = new string[1,3];
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                for (int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i, j] = "*";
                }
            }
            arr.PrintArrayInfo();


            string[] str = { "赤橙黄绿","蓝靛紫"};
            foreach (var color in str)
            {
                Console.WriteLine(color);
            }


            string strArray = "银装素裹,火树银花,银海生花";
            var strInfo = strArray.Split("银");
            strInfo.PrintArrayInfo();


            string array = "0?(13|14|15|18|17)[0-9]{9}";
            char[] charArray = { '0', '?' };
            var regularExpressionArray = array.Split(charArray);
            regularExpressionArray.PrintArrayInfo();
        }
    }
}
