﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp16
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = { 15, 25, 35, 45, 55 };
            for (int i = 0; i < a.Length; i = i + 1)
            {
                Console.WriteLine(a[i]);
            }


            double[] points = { 58, 68, 78, 88, 98 };
            double sum = 0;
            double avg = 0;
            foreach (double point in points)
            {
                sum = sum + point;
            }
            avg = sum / points.Length;
            Console.WriteLine(sum);
            Console.WriteLine(avg );


            string str = "aaajbbbkcccjdddkeee";
            String[] sArray = str.Split(new char[2] { 'j', 'k' });
            foreach (string i in sArray)
            {
                Console.WriteLine(i.ToString() + "");
            }
        }
    }
}
