﻿using System;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        { 
            //数组的定义
            double[,] student = { { 175, 110 }, { 180, 140 }, { 190,160  } };
            for (int i = 0; i <student.GetLength(0); i++)
            {
                Console.WriteLine("第" + (i + 1) + "位同学的身高体重：");
                for (int j = 0; j < student.GetLength(1); j++)
                {
                    Console.Write(student[i, j] + "，");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            //foreach
            string str = "福建，广东，广西";
            foreach(var item in str)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();

            //spilt
            string str1 = "前程似锦，未来可期";
            var firstArray = str1.Split('，');
            firstArray.PrintArraysInfo();
            Console.WriteLine();


            string str2 = "这 可 以 去 掉 空 格";
            string[] strArr = str2.Split(' ');
            foreach (var item in strArr)
            {
                Console.Write(item);
            }
        }
    }
}
