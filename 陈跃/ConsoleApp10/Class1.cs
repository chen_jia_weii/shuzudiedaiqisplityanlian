﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp10
{
    public static class PrintMethod
    {  
        public static void PrintArraysInfo(this string[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
        }
    }
}
